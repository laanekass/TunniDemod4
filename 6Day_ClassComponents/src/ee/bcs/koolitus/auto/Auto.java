package ee.bcs.koolitus.auto;

public class Auto {
	private int usteArv;
	private int kohtadeArv;
	private Mootor mootor;
	private String voti;
	private int id;
	private static int idCounter = 0;

	public Auto() {
		idCounter++;
		id = idCounter;
		System.out.println("Tegin uue auto objekti");
	}

	public Auto(Mootor mootor) {
		this();
		this.mootor = mootor;
	}

	public void kaivitaMootor(String votiId,
			KytuseTyyp kytuseTyyp) {
		if (votiId.equals(this.voti)) {
			System.out.println("Pane võti süütelukku");
			kaivitaMootor(kytuseTyyp);
		} else {
			System.out.println("Vale auto");
		}
	}

	public void kaivitaMootor(KytuseTyyp kytuseTyyp) {
		if (kytuseTyyp.equals(KytuseTyyp.DIISEL)) {
			System.out.println("Käivita eelsüüde");
		}
		System.out.println("Käivita starter");
	}

	public int getUsteArv() {
		return usteArv;
	}

	public void setUsteArv(int usteArv) {
		this.usteArv = usteArv;
	}

	public int getKohtadeArv() {
		return kohtadeArv;
	}

	public void setKohtadeArv(int kohtadeArv) {
		this.kohtadeArv = kohtadeArv;
	}

	public Mootor getMootor() {
		return mootor;
	}

	public void setMootor(Mootor mootor) {
		this.mootor = mootor;
	}

	public String getVoti() {
		return voti;
	}

	public void setVoti(String voti) {
		this.voti = voti;
	}

	public void setVoti(String vanaVoti, String uusVoti) {
		if (this.voti.equals(vanaVoti)) {
			this.voti = uusVoti;
		} else {
			System.out.println("Vana võti ei sobi");
		}
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Auto[id=" + id + ", usteArv=" + usteArv
				+ ", kohtadeArv=" + kohtadeArv + ", mootor="
				+ mootor + ", voti=" + voti + "]";
	}
}
