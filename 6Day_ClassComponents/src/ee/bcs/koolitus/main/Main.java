package ee.bcs.koolitus.main;

import ee.bcs.koolitus.auto.Auto;
import ee.bcs.koolitus.auto.KytuseTyyp;
import ee.bcs.koolitus.auto.Mootor;

public class Main {

	public static void main(String[] args) {
		Mootor diiselMootor = new Mootor()
				.setKytuseTyyp(KytuseTyyp.DIISEL)
				.setVoimsus(125);
		System.out.println(
				diiselMootor.getClass().getSimpleName());

		// diiselMootor.setKytuseTyyp(KytuseTyyp.DIISEL)
		// .setVoimsus(125);

		Auto skoda = new Auto(diiselMootor);
		skoda.setKohtadeArv(5);
		skoda.setUsteArv(5);
		skoda.setMootor(diiselMootor);

		System.out.println("mootor = " + diiselMootor);
		System.out.println("auto = " + skoda);

		Auto bmw = new Auto();
		bmw.setKohtadeArv(2);
		bmw.setUsteArv(3);
		bmw.setMootor(new Mootor()
				.setKytuseTyyp(KytuseTyyp.BENSIIN)
				.setVoimsus(125));
		bmw.setVoti("bmwKey1");
		System.out.println("auto 2 = " + bmw);
		
		System.out.print("škoda: ");
		skoda.kaivitaMootor(KytuseTyyp.DIISEL);
		System.out.print("BMW: ");
		bmw.kaivitaMootor("bmwKey1", KytuseTyyp.BENSIIN);
		
		bmw.setVoti("bmwKey1", "bmwNewKey1");
		System.out.println("auto 2 = " + bmw);
		bmw.setVoti("bmwKey1", "bmwNewNewKey1");
		System.out.println("auto 2 = " + bmw);
		
		bmw.kaivitaMootor("bmwKeyRandom", KytuseTyyp.BENSIIN);
	}

}
