package ee.bcskoolitus.datatypes;

/**
 * This class is for introducing java data types
 * 
 * @author heleen
 *
 */
public class DataTypesIntro {

	public static void main(String[] args) {
		byte esimeneVaikeByte = 127;
		byte teineVaikeByte = 8;

		System.out.println("Esimene byte = " + esimeneVaikeByte + " ja teine = " + teineVaikeByte);
		byte byteSumma = (byte) (esimeneVaikeByte + teineVaikeByte);
		System.out.println("kahe byte'i summa on " + (byteSumma));

		Byte esimeneSuurByte = 127;
		Byte teineSuurByte = -18;

		System.out.println("Esimene byte = " + esimeneSuurByte + " ja teine = " + teineSuurByte);
		System.out.println("kahe byte'i summa on " + (esimeneSuurByte + teineSuurByte));

		char t2htA = 'a';
		char t2htB = 'B';

		System.out.println("Esimene tähtede trükk = " + t2htA + t2htB);
		System.out.println("Teine tähtede trükk = " + (t2htA + t2htB));
		char kaheChariSumma = (char) (t2htA + t2htB);
		System.out.println("Kolmas tähtede trükk = " + kaheChariSumma);

		Long testLong = 123456L;

		int arv1 = 5;
		System.out.println("arv1 = " + arv1);
		arv1 = arv1 + 2;
		System.out.println("peale esimest tehet arv1 = " + arv1);
		arv1++;
		System.out.println("peale teist tehet arv1 = " + arv1);

		int a1 = 12;
		int b1 = ++a1;
		System.out.println("a1 = " + a1 + "; b1 = " + b1);
		int a2 = 12;
		int b2 = a2++;
		System.out.println("a2 = " + a2 + "; b2 = " + b2);

		int arv11 = 11;
		arv11 += 5; // samaväärne tehtega arv11 = arv11 +5;
		System.out.println("arv11 = " + arv11);

		// hhdfakfjfjs
		// jkdfhsdjfhs
		// lkjlkdfjklslak

		int a = 18 % 3;
		
		String nimi="testNimi";
		
		System.out.println("karu" == "ka".concat("ru"));
		System.out.println("karu".equals("ka".concat("ru")));
		
		
		String nimi1 = "siin on esimene nimi";		
		String nimi2 = nimi1;
		
		nimi1 = "muudan esimest nime";
		nimi1 = "lõplik ".concat("esimene nimi");
		System.out.println("esimene nimi = \"" + nimi1 +"\"");
		System.out.println("teine nimi = \"" + nimi2 +"\"");
		
		
	}
}
