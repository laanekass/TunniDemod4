package ee.bcskoolitus.main;

import java.util.ArrayList;

import ee.bcskoolitus.inheritance.Gender;
import ee.bcskoolitus.inheritance.Human;
import ee.bcskoolitus.inheritance.Student;

public class Main {

	public static void main(String[] args) {
		Human justHuman = new Human("Mari", "Kuusk");
//		justHuman.setFirstName("Mari");
//		justHuman.setLastName("Kuusk");
		justHuman.setGender(Gender.FEMALE);

		Student studentMati = new Student();
		studentMati.setFirstName("Mati");
		studentMati.setLastName("Tamm");
		studentMati.setGender(Gender.MALE);
		studentMati.setGrade("second");

		System.out.println(justHuman);
		System.out.println(studentMati);
		
		System.out.println("all humans: " + Human.humans);
		Human.humans.remove(justHuman);
		System.out.println("all humans after removal: " + Human.humans);
		//Human.humans = new ArrayList(); ei saa sest list on final
		
		Human studentMari = new Student();
		((Student)studentMari).getGrade();
		
		
		Human humanMati = new Human("mati", "t");
		//((Student)humanMati).getGrade();
		
		Human test=null;
		
		if(test instanceof Human) {
			System.out.println("mati on Õpilane");
		} else {
			System.out.println("Mati ei ole õpilane");
		}
		
	}

}
