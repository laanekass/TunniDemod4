package ee.bcskoolitus.inheritance;

public class Student extends Human {
	private String grade;

	public Student() {
		super("Mari", "");
	}

	public Student(String firstName, String lastName,
			String grade) {
		super(firstName, lastName);		
		this.grade = grade;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Override
	public String toString() {
		return "Student[firstName=" + getFirstName()
				+ ", lastName=" + getLastName()
				+ ", gender=" + getGender() + ", grade="
				+ grade + "]";
	}
}
