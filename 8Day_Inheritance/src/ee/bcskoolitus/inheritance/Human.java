package ee.bcskoolitus.inheritance;

import java.util.ArrayList;
import java.util.List;

public class Human {
	private String firstName;
	private String lastName;
	private Gender gender;
	public final static List<Human> humans = new ArrayList<>();

	public Human(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		humans.add(this);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Human[firstName=" + firstName
				+ ", lastName=" + lastName + ", gender="
				+ gender + "]";
	}
}
