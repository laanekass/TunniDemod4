package ee.bcskoolitus.inheritance;

public class Teacher extends Human {
	private String classRoomNumber;

	public Teacher(String firstName, String lastName) {
		super(firstName, lastName);
	}

	public String getClassRoomNumber() {
		return classRoomNumber;
	}

	public void setClassRoomNumber(String classRoomNumber) {
		this.classRoomNumber = classRoomNumber;
	}

	@Override
	public String toString() {
		return "Human[firstName=" + getFirstName()
				+ ", lastName=" + getLastName()
				+ ", gender=" + getGender()
				+ ", classRoomNumber" + classRoomNumber
				+ "]";
	}
}
