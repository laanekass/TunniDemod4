package ee.bcskoolitus.car;

import java.util.Objects;

public class Car implements Comparable<Car> {
	private int numberOfDoors;
	private int numberOfSeats;
	private Engine engine;

	public int getNumberOfDoors() {
		return numberOfDoors;
	}

	public void setNumberOfDoors(int numberOfDoors)
			throws IncorrectNumberOfDoorsException {
		if (numberOfDoors > 1 && numberOfDoors <= 7) {
			this.numberOfDoors = numberOfDoors;
		} else {
			throw new IncorrectNumberOfDoorsException(
					"Lubatud uste arv jääb vahemikku 2-7");
		}
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	@Override
	public boolean equals(Object otherObject) {
		if (this == otherObject) {
			return true;
		}
		if (otherObject == null) {
			return false;
		}

		if (getClass() != otherObject.getClass()) {
			return false;
		}

		final Car otherCar = (Car) otherObject;
		return this.getNumberOfDoors() == otherCar
				.getNumberOfDoors()
				&& this.getNumberOfSeats() == otherCar
						.getNumberOfSeats()
				&& this.getEngine()
						.equals(otherCar.getEngine());
	}

	@Override
	public int hashCode() {
		int hash = 231;
		hash = 29 * hash + Objects.hashCode(numberOfDoors);
		hash = 29 * hash + Objects.hashCode(numberOfSeats);
		hash = 29 * hash + Objects.hashCode(engine);
		return hash;
	}

	@Override
	public int compareTo(Car otherCar) {
		if (this.equals(otherCar)) {
			return 0;
		} else if (this.numberOfDoors < otherCar
				.getNumberOfDoors()) {
			return -1;
		} else if (this.numberOfSeats < otherCar
				.getNumberOfSeats()) {
			return -1;
		} else if (this.engine
				.compareTo(otherCar.getEngine()) < 0) {
			return -1;
		}
		return 1;
	}
}
