package ee.bcskoolitus.car;

import java.util.Objects;

public class Engine implements Comparable<Engine> {
	private int power;
	private FuelType fuelType;

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public FuelType getFuelType() {
		return fuelType;
	}

	public void setFuelType(FuelType fuelType) {
		this.fuelType = fuelType;
	}

	@Override
	public boolean equals(Object otherObject) {
		if (this == otherObject) {
			return true;
		}
		if (otherObject == null) {
			return false;
		}

		if (getClass() != otherObject.getClass()) {
			return false;
		}
		final Engine otherEngine = (Engine) otherObject;
		return this.getFuelType()
				.equals(otherEngine.getFuelType())
				&& this.getPower() == otherEngine
						.getPower();
	}

	@Override
	public int hashCode() {
		int hash = 87;
		hash = 31 * hash + Objects.hashCode(fuelType);
		hash = 31 * hash + Objects.hashCode(power);
		return hash;
	}

	@Override
	public int compareTo(Engine otherEngine) {
		if (this.equals(otherEngine)) {
			return 0;
		} else if (this.fuelType.compareTo(
				otherEngine.getFuelType()) != 0) {
			return this.fuelType
					.compareTo(otherEngine.getFuelType());
		} else if (this.power <= otherEngine.power) {
			return -1;
		}
		return 1;
	}
}
