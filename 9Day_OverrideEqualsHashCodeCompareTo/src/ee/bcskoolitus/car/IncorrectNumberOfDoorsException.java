package ee.bcskoolitus.car;

public class IncorrectNumberOfDoorsException
		extends Exception {

	public IncorrectNumberOfDoorsException() {
		super();
	}
	
	public IncorrectNumberOfDoorsException(String message) {
		super(message);
	}
}
