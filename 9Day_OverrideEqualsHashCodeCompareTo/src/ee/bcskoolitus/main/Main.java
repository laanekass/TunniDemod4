package ee.bcskoolitus.main;

import ee.bcskoolitus.car.Car;
import ee.bcskoolitus.car.Engine;
import ee.bcskoolitus.car.FuelType;
import ee.bcskoolitus.car.IncorrectNumberOfDoorsException;

public class Main {

	public static void main(String[] args) {
		Engine engine1 = new Engine();
		engine1.setFuelType(FuelType.DIESEL);
		engine1.setPower(84);

		Car car1 = new Car();
		try {
			car1.setNumberOfDoors(10);
		} catch (IncorrectNumberOfDoorsException e) {
			System.out.println(e.getMessage());
		}
		car1.setNumberOfSeats(2);
		car1.setEngine(engine1);

		Engine engine2 = new Engine();
		engine2.setFuelType(FuelType.DIESEL);
		engine2.setPower(84);

		Car car2 = new Car();
		try {
			car2.setNumberOfDoors(3);
		} catch (IncorrectNumberOfDoorsException e) {
			System.out.println(e.getMessage());
		}
		car2.setNumberOfSeats(2);
		car2.setEngine(engine2);

		System.out.println(
				"is car1 = car2: " + car1.equals(car2));
		System.out.println(car1.hashCode());
		System.out.println("is car1 hash = car2 hash: "
				+ (car1.hashCode() == car2.hashCode()));
	}

}
