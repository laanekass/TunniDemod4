package test.ee.bcskoolitus.car;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ee.bcskoolitus.car.Engine;
import ee.bcskoolitus.car.FuelType;

public class EngineTest {
	private static final int OrderSwitched = 1;
	private static final int OrderKept = -1;
	Engine petrolEngineSmallPower;
	Engine dieselEngineSmallPower;
	Engine petrolEngineMorePower;
	Engine dieselEngineMorePower;

	@Before
	public void setUp() {
		petrolEngineSmallPower = new Engine();
		petrolEngineSmallPower.setFuelType(FuelType.PETROL);
		petrolEngineSmallPower.setPower(64);
		
		petrolEngineMorePower = new Engine();
		petrolEngineMorePower.setFuelType(FuelType.PETROL);
		petrolEngineMorePower.setPower(125);
		
		dieselEngineSmallPower = new Engine();
		dieselEngineSmallPower.setFuelType(FuelType.DIESEL);
		dieselEngineSmallPower.setPower(64);
		
		dieselEngineMorePower = new Engine();
		dieselEngineMorePower.setFuelType(FuelType.DIESEL);
		dieselEngineMorePower.setPower(125);
	}

	@Test
	public void testWhenDieselBeforePetrolPowerSameOrderKept() {
		Assert.assertEquals(OrderKept, dieselEngineMorePower.compareTo(petrolEngineMorePower));		
	}
	
	@Test
	public void testWhenDieselAfterPetrolPowerSameOrderSwitched() {
		Assert.assertEquals(OrderSwitched, petrolEngineMorePower.compareTo(dieselEngineMorePower));		
	}
	
	@Test
	public void testWhenDieselMorePowerBeforeDieselSmallPowerOrderSwitched() {
		Assert.assertEquals(OrderSwitched, dieselEngineMorePower.compareTo(dieselEngineSmallPower));		
	}
	
	@Test
	public void testWhenDieselMorePowerAfterDieselSmallPowerOrderKept() {
		Assert.assertEquals(OrderKept, dieselEngineSmallPower.compareTo(dieselEngineMorePower));		
	}
}
