package main.java.ee.bcskoolitus.testrest.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/testcontrollers")
public class TestController {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }

    @GET
    @Path("/test1/{test1}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt1(@PathParam("test1") String test1) {
        return "Got it! - " + test1;
    }

    @GET
    @Path("/test2/{test2}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt2(@PathParam("test2") String test2) {
        return "Got it! - " + test2;
    }
}
