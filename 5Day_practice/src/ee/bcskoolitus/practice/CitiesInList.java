package ee.bcskoolitus.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CitiesInList {

	public static void main(String[] args) {
		List<List<String>> citiesInList = new ArrayList<>();
		citiesInList.add(
				new ArrayList<>(Arrays.asList("Helsinki",
						"Helsingi", "Helsinki", "Soome")));
		citiesInList.add(new ArrayList<>(Arrays.asList(
				"Tallinn", "Tallinn", "Tallinn", "Eesti")));
		citiesInList.add(new ArrayList<>(
				Arrays.asList("Copenhagen", "Kopenhaagen",
						"København", "Taani")));
		citiesInList.add(new ArrayList<>(
				Arrays.asList("Stockholm", "Stockholm",
						"Stockholm", "Rootsi")));
		citiesInList.add(new ArrayList<>(
				Arrays.asList("London", "London", "London",
						"Ühendkuningriik")));

		for (List<String> countryCities : citiesInList) {
			System.out.println(countryCities.get(1));
		}
		System.out.println("---------- ül - 5 ---------");
		for (List<String> countryCities : citiesInList) {
			System.out.println(String.format(
					"Riik - %s: pealinn - %s; inglise keeles - %s, "
							+ "kohalikus keeles - %s.",
					countryCities.get(3),
					countryCities.get(1),
					countryCities.get(0),
					countryCities.get(2)));
		}

		System.out.println("---------- ül - 6 ---------");
		// liigutan riigi esimeseks ja kustutan selle lõpust ära
		for (List<String> countryCities : citiesInList) {
			countryCities.add(0, countryCities.get(3));
			countryCities.remove(4);
		}
		// lisan vajalikele riikidele linnanimed lõppu
		citiesInList.get(0).add("Helsingfors");
		// trükkimine
		for (List<String> countryCities : citiesInList) {
			String localNames = countryCities.get(3);
			if (countryCities.size() > 4) {
				for (int cityIndex = 4; cityIndex < countryCities
						.size(); cityIndex++) {
					localNames += "; "
							+ countryCities.get(cityIndex);
				}
			}
			System.out.println(String.format(
					"Riik - %s: pealinn - %s; inglise keeles - %s, "
							+ "kohalikus keeles - %s.",
					countryCities.get(0),
					countryCities.get(2),
					countryCities.get(1), localNames));
		}
		System.out.println("---------- ül - 7 ---------");
		for (List<String> countryCities : citiesInList) {
			countryCities.set(0, "country:"
					.concat(countryCities.get(0)));
		}

		System.out.println("---------- ül - 8 ---------");
		for (List<String> countryCities : citiesInList) {
			String localNames = countryCities.get(3);
			if (countryCities.size() > 4) {
				for (int cityIndex = 4; cityIndex < countryCities
						.size(); cityIndex++) {
					localNames += "; "
							+ countryCities.get(cityIndex);
				}
			}
			System.out.println(String.format(
					"Riik - %s: pealinn - %s; inglise keeles - %s, "
							+ "kohalikus keeles - %s.",
					countryCities.get(0).split(":")[1],
					countryCities.get(2),
					countryCities.get(1), localNames));
		}
		System.out.println("---------- ül - 9 ---------");
		citiesInList.add(2, new ArrayList<>(Arrays.asList(
				"country:Kurrunurruvutisaare kuningriik",
				"LongStocking City", "Pikksuka linn",
				"Langstrump")));
		System.out.println(citiesInList);
		
		System.out.println("---------- ül - 10 ---------");
		citiesInList.remove(2);
		System.out.println(citiesInList);
	}
}
