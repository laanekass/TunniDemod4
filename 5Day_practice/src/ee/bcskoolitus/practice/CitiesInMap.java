package ee.bcskoolitus.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CitiesInMap {

	public static void main(String[] args) {
		Map<String, List<String>> citiesInMap = new TreeMap<>();
		citiesInMap.put("Soome",
				new ArrayList<>(Arrays.asList("Helsinki",
						"Helsingi", "Helsinki")));
		citiesInMap.put("Eesti", new ArrayList<>(Arrays
				.asList("Tallinn", "Tallinn", "Tallinn")));
		citiesInMap.put("Taani",
				new ArrayList<>(Arrays.asList("Copenhagen",
						"Kopenhaagen", "København")));
		citiesInMap.put("Rootsi",
				new ArrayList<>(Arrays.asList("Stockholm",
						"Stockholm", "Stockholm")));
		citiesInMap.put("Ühendkuningriik",
				new ArrayList<>(Arrays.asList("London",
						"London", "London")));
		for(List<String> cities: citiesInMap.values()) {
			System.out.println(cities.get(1));
		}
		
		System.out.println("---------- ül - 5 ---------");
		for (String key : citiesInMap.keySet()) {
			List<String> countryCities = citiesInMap.get(key);
			System.out.println(String.format(
					"Riik - %s: pealinn - %s; inglise keeles - %s, "
							+ "kohalikus keeles - %s.",
					key,
					countryCities.get(1),
					countryCities.get(0),
					countryCities.get(2)));
		}
	}

}
