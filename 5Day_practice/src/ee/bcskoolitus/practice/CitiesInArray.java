package ee.bcskoolitus.practice;

import java.util.Arrays;

public class CitiesInArray {
	public static void main(String[] args) {
		String[][] citiesInArray = new String[195][4];
		String[] finland = { "Helsinki", "Helsingi",
				"Helsinki", "Soome" };
		String[] estonia = { "Tallinn", "Tallinn",
				"Tallinn", "Eesti" };
		String[] denmark = { "Copenhagen", "Kopenhaagen",
				"København", "Taani" };
		String[] sweden = { "Stockholm", "Stockholm",
				"Stockholm", "Rootsi" };
		String[] uk = { "London", "London", "London",
				"Ühendkuningriik" };
		citiesInArray[0] = finland;
		citiesInArray[1] = estonia;
		citiesInArray[2] = denmark;
		citiesInArray[3] = sweden;
		citiesInArray[4] = uk;

		System.out.println("-----------ül 4. ------------");
		final int CITY_NAME_IN_ESTONIAN = 1;
		for (int row = 0; row < citiesInArray.length; row++) {
			if (citiesInArray[row][0] != null
					&& citiesInArray[row][0] != "") {
				System.out.println(
						citiesInArray[row][CITY_NAME_IN_ESTONIAN]);
			}
		}

		System.out.println(
				"-----------ül 5. keerulisem variant ------------");
		for (int row = 0; row < citiesInArray.length; row++) {
			if (citiesInArray[row][0] != null
					&& citiesInArray[row][0] != "") {
				System.out.println(String.format(
						"Riik - %s: pealinn - %s; inglise keeles - %s, "
								+ "kohalikus keeles - %s.",
						citiesInArray[row][3],
						citiesInArray[row][CITY_NAME_IN_ESTONIAN],
						citiesInArray[row][0],
						citiesInArray[row][2]));
			}
		}

		System.out.println("-----------ül 6. ------------");
		// liigutan linna esimeseks
		for (int row = 0; row < citiesInArray.length; row++) {
			if (citiesInArray[row][0] != null
					&& citiesInArray[row][0] != "") {
				String[] newTempRow = {
						citiesInArray[row][3],
						citiesInArray[row][0],
						citiesInArray[row][1],
						citiesInArray[row][2] };
				citiesInArray[row] = newTempRow;
			}
		}
		// lisan riikidele, millel on mitu riikliku keelt
		// puuduvad nimed
		String[] newFinland = { "Soome", "Helsinki",
				"Helsingi", "Helsinki", "Helsingfors" };
		citiesInArray[0] = newFinland;

		// trükin lauseid nii, et oleks arvestatud erineva arvu kohalike keeltega
		for (int row = 0; row < citiesInArray.length; row++) {
			if (citiesInArray[row][0] != null
					&& citiesInArray[row][0] != "") {
				String localNames = citiesInArray[row][3];
				if (citiesInArray[row].length > 4) {
					for (int localNamesContinueIndex = 4; localNamesContinueIndex < citiesInArray[row].length; localNamesContinueIndex++) {
						localNames += "; "
								+ citiesInArray[row][localNamesContinueIndex];
					}
				}
				System.out.println(String.format(
						"Riik - %s: pealinn - %s; inglise keeles - %s, "
								+ "kohalikus keeles - %s.",
						citiesInArray[row][0],
						citiesInArray[row][1],
						citiesInArray[row][2], localNames));
			}
		}

		// System.out.println("-----------ül 7. ------------");
		// lisan kõigile mitte-null ridadele riigi nimele "country:"
		for (String[] row : citiesInArray) {
			if (row[0] != null && row[0] != "") {
				row[0] = "country:" + row[0];
			}
		}

		System.out.println("-----------ül 8. ------------");
		// tsükkel eelmisest punktist, vajab ainult linna nime korrigeerimist
		printCitiesInfo(citiesInArray);

		System.out.println("-----------ül 9. ------------");
		// nihutan järgmised read ühe võrra edasi
		for (int row = citiesInArray.length - 1; row >= 2; row--) {
			if (citiesInArray[row][0] != null
					&& citiesInArray[row][0] != "") {
				citiesInArray[row + 1] = citiesInArray[row];
			}
		}

		// lisan kolmandasse ritta Kurrunurru
		String[] kurrunurru = {
				"country:Kurrunurruvutisaare Kuningriik",
				"Longstocking City", "Pikksuka linn",
				"Langstrump" };
		citiesInArray[2] = kurrunurru;
		// System.out.println(Arrays.deepToString(citiesInArray));
		printCitiesInfo(citiesInArray);

		System.out.println("-----------ül 10. -----------");
		// kirjutan alates 4. reast kirjed ühe rea varasemateks
		for (int row = 3; row < citiesInArray.length; row++) {
			citiesInArray[row-1] = citiesInArray[row];
		}
		printCitiesInfo(citiesInArray);
	}

	private static void printCitiesInfo(
			String[][] citiesInArray) {
		for (int row = 0; row < citiesInArray.length; row++) {
			if (citiesInArray[row][0] != null
					&& citiesInArray[row][0] != "") {
				String localNames = citiesInArray[row][3];
				if (citiesInArray[row].length > 4) {
					for (int localNamesContinueIndex = 4; localNamesContinueIndex < citiesInArray[row].length; localNamesContinueIndex++) {
						localNames += "; "
								+ citiesInArray[row][localNamesContinueIndex];
					}
				}
				System.out.println(String.format(
						"Riik - %s: pealinn - %s; inglise keeles - %s, "
								+ "kohalikus keeles - %s.",
						citiesInArray[row][0].split(":")[1],
						citiesInArray[row][1],
						citiesInArray[row][2], localNames));
			}
		}
	}
}
