package ee.bcskoolitus.main;

import ee.bcskoolitus.animal.Animal;
import ee.bcskoolitus.animal.Dog;
import ee.bcskoolitus.animal.Gender;

public class Main {

	public static void main(String[] args) {
		// Animal a = new Animal(); ei saa teha kuna klass on abstraktne
		Dog kiki = new Dog();
		kiki.setGender(Gender.FEMALE);
		kiki.setSleepingPlace("travelbag");
		kiki.setSpeciesName(
				kiki.getClass().getSimpleName());
		kiki.moveAhead();
		System.out.println("Kiki is a " + kiki.getSpeciesName());
	}

}
