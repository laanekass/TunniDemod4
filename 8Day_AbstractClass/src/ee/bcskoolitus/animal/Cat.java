package ee.bcskoolitus.animal;

public class Cat extends Animal {
	private int sleepingTimeInHours;

	@Override
	public void moveAhead() {
		System.out.println("Sneaking low");
	}

	public int getSleepingTimeInHours() {
		return sleepingTimeInHours;
	}

	public void setSleepingTimeInHours(
			int sleepingTimeInHours) {
		this.sleepingTimeInHours = sleepingTimeInHours;
	}
}
