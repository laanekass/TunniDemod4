package ee.bcskoolitus.animal;

public abstract class Animal {
	private String speciesName;
	private Gender gender;
	
	public abstract void moveAhead();
	
	public String getSpeciesName() {
		return speciesName;
	}
	public void setSpeciesName(String speciesName) {
		this.speciesName = speciesName;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
}
