package ee.bcslppöotus.recursivemethod;

public class RecursiveMethodDemo {

	public static void main(String[] args) {
		System.out.println(factorial(3));
		System.out.println(factorial1(3));
		System.out.println(sumOfVarargs(2, 5, 8, 43, 3, 2));
		System.out.println(sumOfVarargs(2, 2));
		System.out.println(
				sumOfVaragrs("Smith", 16, 45, 50, 5));
		System.out.println(
				averageAgeOfFamily("Smith", 16, 45, 50, 5));
	}

	public static int factorial(int x) {
		System.out.println(x);
		if (x <= 1) {
			return x;
		} else {
			return factorial(x - 1) * x;
		}
	}

	public static int factorial1(int x) {
		return (x <= 1) ? x : factorial1(x - 1) * x;
	}

	public static int sumOfVarargs(int... numbers) {
		int sum = 0;
		for (int singleNumber : numbers) {
			sum += singleNumber;
		}
		return sum;
	}

	public static String sumOfVaragrs(String familyName,
			int... numbers) {
		return familyName + "'s sum of age is "
				+ sumOfVarargs(numbers);
	}

	public static String averageAgeOfFamily(
			String familyName, int... numbers) {
		return familyName + "'s avergae age is "
				+ (sumOfVarargs(numbers) / numbers.length);
	}
}
