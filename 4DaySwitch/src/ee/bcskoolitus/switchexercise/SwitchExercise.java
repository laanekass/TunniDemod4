package ee.bcskoolitus.switchexercise;

public class SwitchExercise {

	public static void main(String[] args) {
		String givenColor = args[0];
		final String GREEN = "green";
		final String RED = "red";
		final String YELLOW = "yellow";

		if (givenColor.equalsIgnoreCase(GREEN)) {
			System.out.println("Driver can drive a car.");
		} else if (givenColor.equalsIgnoreCase(RED)) {
			System.out.println("Driver has to stop car and wait for green light");
		} else if (givenColor.equalsIgnoreCase(YELLOW)) {
			System.out.println("Driver has to be ready to stop car or to start driving");
		} else {
			System.out.println("Driver should not be driving");
		}

		switch (givenColor.toLowerCase()) {
		case GREEN:
			System.out.println("Driver can drive a car.");
			break;
		case RED:
			System.out.println("Driver has to stop car and wait for green light");
			break;
		case YELLOW:
			System.out.println("Driver has to be ready to stop car or to start driving");
			break;
		default:
			System.out.println("Driver should not be driving");
		}

	}

}
