package ee.bcskoolitus.collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetDemo {

	public static void main(String[] args) {
		Set<Double> numberSet = new HashSet<>();
		numberSet.add(2.0);
		numberSet.add(20.0);
		numberSet.add(15.5);
		numberSet.add(15.5);
		numberSet.add(0.005);
		System.out.println(numberSet);
		
		Set<Double> numberTreeSet = new TreeSet<>();
		numberTreeSet.add(2.0);
		numberTreeSet.add(20.0);
		numberTreeSet.add(15.5);
		numberTreeSet.add(15.5);
		numberTreeSet.add(0.005);
		System.out.println(numberTreeSet);
	}
}
