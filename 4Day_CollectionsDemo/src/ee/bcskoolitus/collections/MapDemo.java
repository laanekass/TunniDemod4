package ee.bcskoolitus.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapDemo {

	public static void main(String[] args) {
		Map<Integer, List<String>> studentsInClasses = new HashMap<>();
		studentsInClasses.put(1, Arrays.asList("mati", "kati", "mari"));
		
		List<String> secondClassStudents = new ArrayList<>();
		secondClassStudents.add("joosep");
		secondClassStudents.add("juhan");
		secondClassStudents.add("elisabet");
		studentsInClasses.put(2, secondClassStudents);
		
		System.out.println(studentsInClasses);
		System.out.println("------Keys-------");
		for(Integer key: studentsInClasses.keySet()) {
			System.out.println(key);
		}
		
		System.out.println("------values-------");
		for(List<String> value: studentsInClasses.values()) {
			System.out.println(value);
		}
		System.out.println("------only second class students-------");
		System.out.println(studentsInClasses.get(2));
		
		System.out.println("------adding new student to second class-------");
		studentsInClasses.get(2).add("marion");
		System.out.println(studentsInClasses.get(2));
	}

}
