package ee.bcskoolitus.main;

import ee.bcskoolitus.animal.Ara;
import ee.bcskoolitus.animal.Dog;

public class Main {
	
	public static void main(String[] args) {
		Dog kiki = new Dog();
		kiki.moveAhead();
		
		Ara popi = new Ara();
		popi.moulting();
		popi.singing();
		popi.moveAhead();
	}
}
