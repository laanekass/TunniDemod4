package ee.bcskoolitus.animal;

public class Ara implements AnimalInt, BirdInt {

	@Override
	public void moveAhead() {
		System.out.println("Flying high");
	}

	@Override
	public void singing() {
		System.out.println("Shrieks");
		
	}

	@Override
	public void moulting() {
		System.out.println("Looks nice again now");
	}

}
