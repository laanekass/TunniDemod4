INSERT INTO `koolitused`.`course_type` (`course_type_name`, `course_type_description`) VALUES ('Seminar', '1-5 päevane koolitus praktiliste harjutustega');
INSERT INTO `koolitused`.`course_type` (`course_type_name`, `course_type_description`) VALUES ('Lühiloeng', '1-2 päevane loengu vormis koolitus');
INSERT INTO `koolitused`.`course_type` (`course_type_name`, `course_type_description`) VALUES ('Loengusari', '6 kuu jooksul perioodiliselt toimuvad loengud ja/või seminarid');
INSERT INTO `koolitused`.`course_type` (`course_type_name`, `course_type_description`) VALUES ('Eksami ettevalmistus', '1-5 päevane eksamik ettevalmistav koolitus');
INSERT INTO `koolitused`.`course_type` (`course_type_name`, `course_type_description`) VALUES ('Töötuba', '1-5 päevane praktiline koolitus');
