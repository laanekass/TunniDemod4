DROP TABLE IF EXISTS course_type;
CREATE TABLE course_type (
  course_type_id int(11) NOT NULL AUTO_INCREMENT,
  course_type_name varchar(45) NOT NULL,
  course_type_description varchar(200) DEFAULT NULL,
  PRIMARY KEY (course_type_id),
  UNIQUE KEY course_name_UNIQUE (course_type_name)
) 