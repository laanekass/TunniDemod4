DROP TABLE IF EXISTS course;
CREATE TABLE course (
  course_id int(11) NOT NULL AUTO_INCREMENT,
  course_name varchar(45) NOT NULL,
  course_description varchar(200) DEFAULT NULL,
  start_date datetime DEFAULT NULL,
  end_date datetime DEFAULT NULL,
  course_type_id int(1) DEFAULT NULL,
  PRIMARY KEY (course_id),
  KEY FK_course_course_type_id_idx (course_type_id),
  CONSTRAINT FK_course_course_type_id FOREIGN KEY (course_type_id) REFERENCES course_type (course_type_id) ON DELETE CASCADE ON UPDATE CASCADE
)