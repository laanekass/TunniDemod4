DROP TABLE IF EXISTS lecturer;
CREATE TABLE lecturer (
  lecturer_id int(11) NOT NULL AUTO_INCREMENT,
  personal_id_code varchar(11) DEFAULT NULL,
  first_name varchar(45) NOT NULL,
  last_name varchar(45) NOT NULL,
  birthday date DEFAULT NULL,
  highest_education int(1) NOT NULL,
  PRIMARY KEY (lecturer_id),
  KEY FK_lecturer_education_level_idx (highest_education),
  CONSTRAINT FK_lecturer_education_level FOREIGN KEY (highest_education) REFERENCES education_level (education_level_id) ON DELETE CASCADE ON UPDATE CASCADE
)