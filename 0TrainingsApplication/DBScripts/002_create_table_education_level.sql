DROP TABLE IF EXISTS education_level;

CREATE TABLE education_level (
  education_level_id int(11) NOT NULL AUTO_INCREMENT,
  education_level_name varchar(45) NOT NULL,
  PRIMARY KEY (education_level_id)
) 