DROP TABLE IF EXISTS course_lecturer;
CREATE TABLE course_lecturer (
  course_id int(11) NOT NULL,
  lecturer_id int(11) NOT NULL,
  KEY FK_course_lecturer_course_id_idx (course_id),
  KEY FK_course_lecturer_lecturer_id_idx (lecturer_id),
  CONSTRAINT FK_course_lecturer_course_id FOREIGN KEY (course_id) REFERENCES course (course_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_course_lecturer_lecturer_id FOREIGN KEY (lecturer_id) REFERENCES lecturer (lecturer_id) ON DELETE CASCADE ON UPDATE CASCADE
)