var allCourses;
function getAllCourses() {
	$.getJSON("http://localhost:8080/TrainingApplication/rest/courses",
			function(courses) {
				allCourses = courses;
				fillCoursesTable(courses);
			});
}

function fillCoursesTable(courses) {
	var tableBody = "";
	for (var i = 0; i < courses.length; i++) {
		tableBody = tableBody
				+ "<tr><td>"
				+ courses[i].courseName
				+ "</td><td>"
				+ courses[i].courseDescription
				+ "</td><td>"
				+ courses[i].courseType.courseTypeName
				+ "</td><td>"
				+ courses[i].startDate.split("T")[0]
				+ "</td><td>"
				+ courses[i].endDate.split("T")[0]
				+ "</td><td>"
				+ getLecturersNamesList(courses[i].lecturers)
				+ "</td><td>"
				+ "<div class='btn-group' role='group' aria-label='course activities'>"
				+ "<button type='button' class='btn btn-secondary btn-md' data-toggle='modal' data-target='#modifyCourseModal'>Modify</button>"
				+ "<button type='button' class='btn btn-secondary btn-md' data-toggle='modal' data-target='#deleteCourseModal' onClick='setDeleteModalContent("
				+ courses[i].courseId + ")'>Remove</button>"
				+ "</div></td></tr>"
	}

	document.getElementById("coursesTableBody").innerHTML = tableBody;
}

function getLecturersNamesList(lecturers) {
	var lecturersNames = "";
	if (lecturers.length > 0) {
		lecturersNames = lecturers[0].firstName + " " + lecturers[0].lastName;
		if (lecturers.length > 1) {
			for (var i = 1; i < lecturers.length; i++) {
				lecturersNames = lecturersNames + ", " + lecturers[i].firstName
						+ " " + lecturers[i].lastName;
			}
		}
	}
	return lecturersNames;
}

getAllCourses()

function prepareAddCourseModal() {
	getAllCourseTypes.then(function(allCourseTypesList) {
		fillCourseTypesSelect(allCourseTypesList);
	});

	getAllLecturers.then(function(allLecturersList) {
		fillLecturersSelect(allLecturersList);
	});
}

function fillCourseTypesSelect(allCourseTypesList) {
	var courseTypesOptions = "";
	for (var i = 0; i < allCourseTypesList.length; i++) {
		courseTypesOptions = courseTypesOptions + "<option value='"
				+ allCourseTypesList[i].courseTypeId + "'>"
				+ allCourseTypesList[i].courseTypeName + "</option>"
	}
	document.getElementById("newCourseTypeSelect").innerHTML = courseTypesOptions;
}

function fillLecturersSelect(allLecturersList) {
	var lecturersOptions = "";
	for (var i = 0; i < allLecturersList.length; i++) {
		lecturersOptions = lecturersOptions + "<option value='"
				+ allLecturersList[i].lecturerId + "'>"
				+ allLecturersList[i].firstName + " "
				+ allLecturersList[i].lastName + "</option>"
	}
	document.getElementById("newCourseLecturerSelect").innerHTML = lecturersOptions;
}

function addNewCourse() {
	var newCourse = {
		"courseName" : document.getElementById("newCourseName").value,
		"courseDescription" : document.getElementById("newCourseDescription").value,
		"startDate" : document.getElementById("newCourseStartDate").value
				+ "T00:00:00+02:00",
		"endDate" : document.getElementById("newCourseEndDate").value
				+ "T00:00:00+02:00",
		"courseType" : {
			"courseTypeId" : document.getElementById("newCourseTypeSelect").value
		},
		"lecturers" : getSelectedLecturers($("#newCourseLecturerSelect").val())
	}

	$.ajax({
		url : "http://localhost:8080/TrainingApplication/rest/courses",
		type : "POST",
		data : JSON.stringify(newCourse),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCourses();
			$('#addCourseModal').modal('toggle');
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on adding new course");
		}
	});
}

function getSelectedLecturers(selectedLecturers) {
	var lecturers = new Array();
	for (var i = 0; i < selectedLecturers.length; i++) {
		lecturers.push({
			"lecturerId" : selectedLecturers[i]
		});
	}
	return lecturers
}

function setDeleteModalContent(courseId) {
	for (var i = 0; i < allCourses.length; i++) {
		if (allCourses[i].courseId == courseId) {
			document.getElementById("removeCourseName").innerHTML = allCourses[i].courseName;
			document.getElementById("removeButton").setAttribute("onClick",
					"removeCourse(" + courseId + ")");
		}
	}
}

function removeCourse(courseId) {
	console.log(courseId);

	$.ajax({
		url : "http://localhost:8080/TrainingApplication/rest/courses/"
				+ courseId,
		type : "DELETE",
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCourses();
			$('#deleteCourseModal').modal('toggle');
		}
	});
}