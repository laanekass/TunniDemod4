var allCourseTypesList;
function getAllCourseTypesAndFillTable() {
	$
			.getJSON(
					"http://localhost:8080/TrainingApplication/rest/coursetypes",
					function(allCourseTypes) {
						allCourseTypesList = allCourseTypes;
						var tableBodyContent = "";
						for (var i = 0; i < allCourseTypes.length; i++) {
							tableBodyContent = tableBodyContent
									+ "<tr><td>"
									+ allCourseTypes[i].courseTypeId
									+ "</td><td>"
									+ allCourseTypes[i].courseTypeName
									+ "</td><td>"
									+ allCourseTypes[i].courseTypeDescription
									+ "</td><td><button type='button' onClick='deleteCourseType("
									+ allCourseTypes[i].courseTypeId
									+ ")'>Delete</button><button type='button' onClick='modifyCourseType("
									+ allCourseTypes[i].courseTypeId
									+ ")'>Modify</button></td></tr>";
						}
						document.getElementById("courseTypesTableBody").innerHTML = tableBodyContent;
					});
}
getAllCourseTypesAndFillTable();

function deleteCourseType(courseTypeId) {
	$.ajax({
		url : "http://localhost:8080/TrainingApplication/rest/coursetypes/"
				+ courseTypeId,
		type : "DELETE",
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCourseTypesAndFillTable();
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on adding new course type");
		}
	});
}

function saveNewCourseType() {
	var jsonData = {
		"courseTypeDescription" : document
				.getElementById("newCourseTypeDescription").value,
		"courseTypeName" : document.getElementById("newCourseTypeName").value
	};

	var jsonDataString = JSON.stringify(jsonData);

	$.ajax({
		url : "http://localhost:8080/TrainingApplication/rest/coursetypes",
		type : "POST",
		data : jsonDataString,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCourseTypesAndFillTable();
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on adding new course type");
		}
	});
}

function modifyCourseType(courseTypeId) {
	for (var i = 0; i < allCourseTypesList.length; i++) {
		if (allCourseTypesList[i].courseTypeId == courseTypeId) {
			document.getElementById("modifyCourseTypeId").value = courseTypeId;
			document.getElementById("modifyCourseTypeName").value = allCourseTypesList[i].courseTypeName;
			document.getElementById("modifyCourseTypeDescription").value = allCourseTypesList[i].courseTypeDescription;
		}
	}
}

function saveCourseTypeChanges() {
	var jsonData = {
		"courseTypeId" : document.getElementById("modifyCourseTypeId").value,
		"courseTypeDescription" : document
				.getElementById("modifyCourseTypeDescription").value,
		"courseTypeName" : document
				.getElementById("modifyCourseTypeName").value
	};

	var jsonDataString = JSON.stringify(jsonData);

	$.ajax({
		url : "http://localhost:8080/TrainingApplication/rest/coursetypes",
		type : "PUT",
		data : jsonDataString,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCourseTypesAndFillTable();
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on adding new course type");
		}
	});
}
