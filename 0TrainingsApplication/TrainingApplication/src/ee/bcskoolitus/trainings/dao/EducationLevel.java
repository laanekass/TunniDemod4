package ee.bcskoolitus.trainings.dao;

public class EducationLevel {
	private int educationLevelId;
	private String educationLevelName;

	public int getEducationLevelId() {
		return educationLevelId;
	}

	public void setEducationLevelId(int educationLevelId) {
		this.educationLevelId = educationLevelId;
	}

	public String getEducationLevelName() {
		return educationLevelName;
	}

	public void setEducationLevelName(String educationLevelName) {
		this.educationLevelName = educationLevelName;
	}

	@Override
	public String toString() {
		return "EducationLevel[eucationLevelId=" + educationLevelId
				+ ", educationLevelName=" + educationLevelName + "]";
	}
}
