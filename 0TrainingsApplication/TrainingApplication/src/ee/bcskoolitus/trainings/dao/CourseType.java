package ee.bcskoolitus.trainings.dao;

public class CourseType {
	private int courseTypeId;
	private String courseTypeName;
	private String courseTypeDescription;

	public int getCourseTypeId() {
		return courseTypeId;
	}

	public void setCourseTypeId(int courseTypeId) {
		this.courseTypeId = courseTypeId;
	}

	public String getCourseTypeName() {
		return courseTypeName;
	}

	public void setCourseTypeName(String courseTypeName) {
		this.courseTypeName = courseTypeName;
	}

	public String getCourseTypeDescription() {
		return courseTypeDescription;
	}

	public void setCourseTypeDescription(String courseTypeDescription) {
		this.courseTypeDescription = courseTypeDescription;
	}

	@Override
	public String toString() {
		return "CourseType[courseTypeId=" + courseTypeId + ", courseTypeName="
				+ courseTypeName + ", courseTypeDescription="
				+ courseTypeDescription + "]";
	}
}
