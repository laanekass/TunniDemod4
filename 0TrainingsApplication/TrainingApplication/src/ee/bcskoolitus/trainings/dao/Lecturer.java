package ee.bcskoolitus.trainings.dao;

import java.util.Date;

public class Lecturer {
	private int lecturerId;
	private String personalIdCode;
	private String firstName;
	private String lastName;
	private Date birtday;
	private EducationLevel highestEducationLevel;

	public int getLecturerId() {
		return lecturerId;
	}

	public void setLecturerId(int lecturerId) {
		this.lecturerId = lecturerId;
	}

	public String getPersonalIdCode() {
		return personalIdCode;
	}

	public void setPersonalIdCode(String personalIdCode) {
		this.personalIdCode = personalIdCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirtday() {
		return birtday;
	}

	public void setBirtday(Date birtday) {
		this.birtday = birtday;
	}

	public EducationLevel getHighestEducationLevel() {
		return highestEducationLevel;
	}

	public void setHighestEducationLevel(EducationLevel highestEducationLevel) {
		this.highestEducationLevel = highestEducationLevel;
	}

	@Override
	public String toString() {
		return "Lecturer[lecturerId=" + lecturerId + ", personalIdCode="
				+ personalIdCode + ", firstName=" + firstName + ", lastName="
				+ lastName + ", birtday=" + birtday + ", highestEducationLevel="
				+ highestEducationLevel + "]";
	}
}
