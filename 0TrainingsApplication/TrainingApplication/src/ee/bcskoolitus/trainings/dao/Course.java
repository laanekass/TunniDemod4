package ee.bcskoolitus.trainings.dao;

import java.util.Date;
import java.util.List;

public class Course {
	private int courseId;
	private String courseName;
	private String courseDescription;
	private Date startDate;
	private Date endDate;
	private CourseType courseType;
	private List<Lecturer> lecturers;

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseDescription() {
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public CourseType getCourseType() {
		return courseType;
	}

	public void setCourseType(CourseType courseType) {
		this.courseType = courseType;
	}

	public List<Lecturer> getLecturers() {
		return lecturers;
	}

	public void setLecturers(List<Lecturer> lecturers) {
		this.lecturers = lecturers;
	}

	@Override
	public String toString() {
		return "Course[courseId=" + courseId + ", courseName=" + courseName
				+ ", courseDescription=" + courseDescription + ", startDate="
				+ startDate + ", endDate=" + endDate + ", courseType="
				+ courseType + "]";
	}
}
