package ee.bcskoolitus.trainings.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.trainings.dao.Course;
import ee.bcskoolitus.trainings.resources.CourseResource;

@Path("/courses")
public class CourseController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Course> getAllCourses() {
		return CourseResource.getAllCourses();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void addNewCourse(Course course) {
		CourseResource.addNewCourse(course);
	}

	@DELETE
	@Path("/{courseId}")
	public void deleteCourse(@PathParam("courseId") int courseId) {
		CourseResource.deleteCourseByCourseId(courseId);
	}
}
