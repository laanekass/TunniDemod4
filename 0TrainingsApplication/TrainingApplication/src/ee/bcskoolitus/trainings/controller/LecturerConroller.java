package ee.bcskoolitus.trainings.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.trainings.dao.Lecturer;
import ee.bcskoolitus.trainings.resources.LecturerResource;

@Path("/lecturers")
public class LecturerConroller {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Lecturer> getAllLecturers() {
		return LecturerResource.getAllLecturers();
	}

}
