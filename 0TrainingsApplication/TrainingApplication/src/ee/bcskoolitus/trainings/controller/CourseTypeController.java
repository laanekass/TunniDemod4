package ee.bcskoolitus.trainings.controller;

import java.awt.TrayIcon.MessageType;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.trainings.dao.CourseType;
import ee.bcskoolitus.trainings.resources.CourseResource;
import ee.bcskoolitus.trainings.resources.CourseTypeResource;

@Path("/coursetypes")
public class CourseTypeController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CourseType> getAllCourseTypes() {
		return CourseTypeResource.getAllCourseTypes();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CourseType addNewCourseType(CourseType newCourseType) {
		return CourseTypeResource.addCourseType(newCourseType);
	}
	
	@DELETE
	@Path("/{courseTypeId}")
	public void deleteCourseTypeById(@PathParam("courseTypeId") int courseTypeId) {
		CourseTypeResource.deleteCourseTypeById(courseTypeId);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public boolean updateCourseType(CourseType courseType) {
		return CourseTypeResource.updateCourseType(courseType);
	}

}
