package ee.bcskoolitus.trainings.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.trainings.dao.CourseType;
import ee.bcskoolitus.trainings.resources.utils.DatabaseConnection;

public abstract class CourseTypeResource {

	public static List<CourseType> getAllCourseTypes() {
		List<CourseType> allCourseTypes = new ArrayList<>();
		String sqlQuery = "SELECT * FROM course_type;";
		try (ResultSet results = DatabaseConnection.getConnection()
				.createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				CourseType courseType = new CourseType();
				courseType.setCourseTypeId(results.getInt("course_type_id"));
				courseType.setCourseTypeName(
						results.getString("course_type_name"));
				courseType.setCourseTypeDescription(
						results.getString("course_type_description"));
				allCourseTypes.add(courseType);
			}
		} catch (SQLException e) {
			System.out.println(
					"Error on getting all course types" + e.getMessage());
		}

		return allCourseTypes;
	}

	public static CourseType getCourseTypeById(int courseTypeId) {
		CourseType courseType = new CourseType();
		String sqlQuery = "SELECT * FROM course_type WHERE course_type_id="
				+ courseTypeId;
		try (ResultSet results = DatabaseConnection.getConnection()
				.createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				courseType.setCourseTypeId(results.getInt("course_type_id"));
				courseType.setCourseTypeName(
						results.getString("course_type_name"));
				courseType.setCourseTypeDescription(
						results.getString("course_type_description"));
			}
		} catch (SQLException e) {
			System.out.println(
					"Error on getting all course types" + e.getMessage());
		}
		return courseType;
	}

	public static CourseType addCourseType(CourseType newCourseType) {
		String sqlQuery = "INSERT INTO course_type "
				+ "(course_type_name, course_type_description) " + "VALUES ('"
				+ newCourseType.getCourseTypeName() + "', '"
				+ newCourseType.getCourseTypeDescription() + "');";
		try (Statement statement = DatabaseConnection.getConnection()
				.createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery,
					Statement.RETURN_GENERATED_KEYS);
			if (resultCode == 1) {
				ResultSet resultSet = statement.getGeneratedKeys();
				while (resultSet.next()) {
					newCourseType.setCourseTypeId(resultSet.getInt(1));
				}
			}

		} catch (SQLException e) {
			System.out.println("Error on adding new course type");
		}
		return newCourseType;
	}

	public static boolean updateCourseType(CourseType courseTypeToUpdate) {
		boolean isUpdateSuccessful = false;
		String sqlQuery = "UPDATE course_type SET course_type_name='"
				+ courseTypeToUpdate.getCourseTypeName()
				+ "', course_type_description='"
				+ courseTypeToUpdate.getCourseTypeDescription()
				+ "' WHERE course_type_id="
				+ courseTypeToUpdate.getCourseTypeId();
		try (Statement statement = DatabaseConnection.getConnection()
				.createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery,
					Statement.RETURN_GENERATED_KEYS);
			if (resultCode == 1) {
				isUpdateSuccessful = true;
			}
		} catch (SQLException e) {
			System.out.println("Error on updating course type");
		}
		return isUpdateSuccessful;
	}

	public static boolean deleteCourseTypeById(int courseTypeId) {
		boolean isDeleteSuccesful = false;
		String sqlQuery = "DELETE FROM course_type " + "WHERE course_type_id="
				+ courseTypeId;
		try (Statement statement = DatabaseConnection.getConnection()
				.createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery);
			if (resultCode == 1) {
				isDeleteSuccesful = true;
			}
		} catch (SQLException e) {
			System.out.println("Error on deleting course type");
		}
		return isDeleteSuccesful;
	}
}
