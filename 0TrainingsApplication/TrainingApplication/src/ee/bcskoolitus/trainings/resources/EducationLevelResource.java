package ee.bcskoolitus.trainings.resources;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ee.bcskoolitus.trainings.dao.EducationLevel;
import ee.bcskoolitus.trainings.resources.utils.DatabaseConnection;

public abstract class EducationLevelResource {
	public static List<EducationLevel> getAllEducationLevels() {
		List<EducationLevel> allEducationLevels = new ArrayList<>();
		String sqlQuery = "SELECT * FROM education_level";
		try (ResultSet results = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery).executeQuery();) {
			while (results.next()) {
				EducationLevel educationLevel = new EducationLevel();
				educationLevel.setEducationLevelId(results.getInt("education_level_id"));
				educationLevel.setEducationLevelName(results.getString("education_level_name"));
				allEducationLevels.add(educationLevel);
			}
		} catch (SQLException e) {
			System.out.println(
					" Error on getting all courses: " + e.getMessage());
		}

		return allEducationLevels;
	}

	public static EducationLevel getEducationLevelById(int educationLevelId) {
		EducationLevel educationLevel = new EducationLevel();
		String sqlQuery = "SELECT * FROM education_level WHERE education_level_id = ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery);) {
			statement.setInt(1, educationLevelId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				educationLevel.setEducationLevelId(results.getInt("education_level_id"));
				educationLevel.setEducationLevelName(
						results.getString("education_level_name"));
			}
		} catch (SQLException e) {
			System.out.println(
					" Error on getting all courses: " + e.getMessage());
		}

		return educationLevel;
	}

}
