package ee.bcskoolitus.trainings.resources;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ee.bcskoolitus.trainings.dao.Lecturer;
import ee.bcskoolitus.trainings.resources.utils.DatabaseConnection;

public abstract class LecturerResource {

	public static List<Lecturer> getAllLecturers() {
		List<Lecturer> allLecturers = new ArrayList<>();
		String sqlQuery = "SELECT * FROM lecturer";
		try (ResultSet results = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery).executeQuery();) {
			while (results.next()) {
				Lecturer lecturer = new Lecturer();
				lecturer.setLecturerId(results.getInt("lecturer_id"));
				lecturer.setPersonalIdCode(
						results.getString("personal_id_code"));
				lecturer.setFirstName(results.getString("first_name"));
				lecturer.setLastName(results.getString("last_name"));
				lecturer.setBirtday(results.getDate("birthday"));
				lecturer.setHighestEducationLevel(
						EducationLevelResource.getEducationLevelById(
								results.getInt("highest_education")));
				allLecturers.add(lecturer);
			}
		} catch (SQLException e) {
			System.out.println(
					" Error on getting all lecturers: " + e.getMessage());
		}
		return allLecturers;
	}

	public static Lecturer getLecturerById(int lecturerId) {
		Lecturer lecturer = new Lecturer();
		String sqlQuery = "SELECT * FROM lecturer WHERE lecturer_id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery)) {
			statement.setInt(1, lecturerId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				lecturer.setLecturerId(results.getInt("lecturer_id"));
				lecturer.setPersonalIdCode(
						results.getString("personal_id_code"));
				lecturer.setFirstName(results.getString("first_name"));
				lecturer.setLastName(results.getString("last_name"));
				lecturer.setBirtday(results.getDate("birthday"));
				lecturer.setHighestEducationLevel(
						EducationLevelResource.getEducationLevelById(
								results.getInt("highest_education")));
			}
		} catch (SQLException e) {
			System.out.println(
					" Error on getting lecturer by id: " + e.getMessage());
		}
		return lecturer;
	}
}
