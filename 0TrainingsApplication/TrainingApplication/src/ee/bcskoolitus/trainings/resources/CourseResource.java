package ee.bcskoolitus.trainings.resources;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import ee.bcskoolitus.trainings.dao.Course;
import ee.bcskoolitus.trainings.resources.utils.DatabaseConnection;
import ee.bcskoolitus.trainings.resources.utils.ResourcesUtils;

public abstract class CourseResource {

	public static List<Course> getAllCourses() {
		List<Course> allCourses = new ArrayList<>();
		String sqlQuery = "SELECT * FROM course";
		try (ResultSet results = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery).executeQuery();) {
			while (results.next()) {
				Course course = new Course();
				course.setCourseId(results.getInt("course_id"));
				course.setCourseName(results.getString("course_name"));
				course.setCourseDescription(
						results.getString("course_description"));
				course.setStartDate(results.getDate("start_date"));
				course.setEndDate(results.getDate("end_date"));
				course.setCourseType(CourseTypeResource
						.getCourseTypeById(results.getInt("course_type_id")));
				course.setLecturers(
						CourseLecturerResource.getCourseLecturersByCourseId(
								results.getInt("course_id")));
				allCourses.add(course);
			}
		} catch (SQLException e) {
			System.out.println(
					" Error on getting all courses: " + e.getMessage());
		}
		return allCourses;
	}

	public static Course getCourseById(int courseId) {
		Course course = new Course();
		String sqlQuery = "SELECT * FROM course ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery);) {
			statement.setInt(1, courseId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				course.setCourseId(results.getInt("course_id"));
				course.setCourseName(results.getString("course_name"));
				course.setCourseDescription(
						results.getString("course_description"));
				course.setStartDate(results.getDate("start_date"));
				course.setEndDate(results.getDate("end_date"));
				course.setCourseType(CourseTypeResource
						.getCourseTypeById(results.getInt("course_type_id")));
				course.setLecturers(
						CourseLecturerResource.getCourseLecturersByCourseId(
								results.getInt("course_id")));
			}
		} catch (SQLException e) {
			System.out
					.println(" Error on getting one course: " + e.getMessage());
		}
		return course;
	}

	public static Course addNewCourse(Course newCourse) {
		if (newCourse.getCourseType().getCourseTypeId() == 0) {
			newCourse.setCourseType(CourseTypeResource
					.addCourseType(newCourse.getCourseType()));
		}
		String sqlQuery = "INSERT INTO course (course_name, course_description, "
				+ "start_date, end_date, course_type_id) VALUES (?, ? ,?, ?, ?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, newCourse.getCourseName());
			statement.setString(2, newCourse.getCourseDescription());
			statement.setDate(3,
					ResourcesUtils.javaDateToSqlDate(newCourse.getStartDate()));
			statement.setDate(4,
					ResourcesUtils.javaDateToSqlDate(newCourse.getEndDate()));
			statement.setInt(5, newCourse.getCourseType().getCourseTypeId());
			if (statement.executeUpdate() == 1) {
				ResultSet results = statement.getGeneratedKeys();
				results.next();
				newCourse.setCourseId(results.getInt(1));				
			}
		} catch (SQLException e) {
			System.out.println("Error on adding new course: " + e.getMessage());
		}
		CourseLecturerResource.addCourseLecturers(newCourse);
		return newCourse;
	}

	public static void updateCourse(Course courseToUpdate) {
		if (courseToUpdate.getCourseType().getCourseTypeId() == 0) {
			courseToUpdate.setCourseType(CourseTypeResource
					.addCourseType(courseToUpdate.getCourseType()));
		}
		String sqlQuery = "UPDATE course "
				+ "SET course_name = ?, course_description= ?, start_date = ?, end_date = ?, course_type_id = ? "
				+ "WHERE course_id = ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery)) {
			statement.setString(1, courseToUpdate.getCourseName());
			statement.setString(2, courseToUpdate.getCourseDescription());
			statement.setDate(3, ResourcesUtils
					.javaDateToSqlDate(courseToUpdate.getStartDate()));
			statement.setDate(4, ResourcesUtils
					.javaDateToSqlDate(courseToUpdate.getEndDate()));
			statement.setInt(5,
					courseToUpdate.getCourseType().getCourseTypeId());
			statement.setInt(6, courseToUpdate.getCourseId());

			statement.executeQuery();
		} catch (SQLException e) {
			System.out.println("Error on updating course: " + e.getMessage());
		}
	}

	public static void deleteCourseByCourseId(int courseId) {
		String sqlQuery = "DELETE FROM course WHERE course_id = ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery)) {
			statement.setInt(1, courseId);

			statement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Error on updating course: " + e.getMessage());
		}
	}

	public static void deleteCourse(Course courseToDelete) {
		String sqlQuery = "DELETE FROM course WHERE course_id = ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery)) {
			statement.setInt(1, courseToDelete.getCourseId());

			statement.executeQuery();
		} catch (SQLException e) {
			System.out.println("Error on updating course: " + e.getMessage());
		}
	}

}
