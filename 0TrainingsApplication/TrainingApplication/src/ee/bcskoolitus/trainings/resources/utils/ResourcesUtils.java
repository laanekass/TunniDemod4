package ee.bcskoolitus.trainings.resources.utils;

import java.util.Date;

public abstract class ResourcesUtils {

	public static java.sql.Date javaDateToSqlDate(Date dateToConevert) {
		return new java.sql.Date(dateToConevert.getTime());
	}
}
