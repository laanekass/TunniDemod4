package ee.bcskoolitus.trainings.resources;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.trainings.dao.Course;
import ee.bcskoolitus.trainings.dao.Lecturer;
import ee.bcskoolitus.trainings.resources.utils.DatabaseConnection;

public abstract class CourseLecturerResource {

	public static List<Lecturer> getCourseLecturersByCourseId(int courseId) {
		List<Lecturer> courseLecturers = new ArrayList<>();
		String sqlQuery = "SELECT * FROM course_lecturer WHERE course_id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery)) {
			statement.setInt(1, courseId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				Lecturer courseLecturer = LecturerResource
						.getLecturerById(results.getInt("lecturer_id"));
				courseLecturers.add(courseLecturer);
			}
		} catch (SQLException e) {
			System.out.println(
					" Error on getting course lecturers: " + e.getMessage());
		}
		return courseLecturers;
	}

	public static List<Course> getLecturerCoursesByLecturerId(int lecturerId) {
		List<Course> lectureCourses = new ArrayList<>();
		String sqlQuery = "SELECT * FROM course_lecturer WHERE lecturer_id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery)) {
			statement.setInt(1, lecturerId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				Course courseLecturer = CourseResource
						.getCourseById(results.getInt("course_id"));
				lectureCourses.add(courseLecturer);
			}
		} catch (SQLException e) {
			System.out.println(
					" Error on getting course lecturers: " + e.getMessage());
		}
		return lectureCourses;
	}

	public static void addCourseLecturers(Course course) {
		for (Lecturer lecturer : course.getLecturers()) {
			addLecturerToCourse(lecturer.getLecturerId(), course.getCourseId());
		}
	}

	private static void addLecturerToCourse(int lecturerId, int courseId) {
		String sqlQuery = "INSERT INTO course_lecturer (course_id, lecturer_id) VALUES (?, ?);";

		try (PreparedStatement statement = DatabaseConnection.getConnection()
				.prepareStatement(sqlQuery);) {
			statement.setInt(1, courseId);
			statement.setInt(2, lecturerId);
			
			System.out.println(statement);
			statement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Error on adding new lecturer course relation: "
					+ e.getMessage());
		}

	}
}
