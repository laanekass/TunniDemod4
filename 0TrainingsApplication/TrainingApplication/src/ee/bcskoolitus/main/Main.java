package ee.bcskoolitus.main;

import ee.bcskoolitus.trainings.dao.CourseType;
import ee.bcskoolitus.trainings.resources.CourseResource;
import ee.bcskoolitus.trainings.resources.CourseTypeResource;

public class Main {

	public static void main(String[] args) {
		System.out.println(CourseTypeResource.getAllCourseTypes());

		System.out.println(CourseResource.getAllCourses());

		System.out.println(CourseTypeResource.getCourseTypeById(2));

		CourseType newCourseType = new CourseType();
		newCourseType.setCourseTypeName("testTyyp");
		newCourseType.setCourseTypeDescription("testyybi kirjeldus");

		System.out.println(CourseTypeResource.addCourseType(newCourseType));

		newCourseType.setCourseTypeName("testTypp peale uuendust");
		System.out.println("was update successful? "
				+ CourseTypeResource.updateCourseType(newCourseType));

		System.out.println("was delete successful? "
				+ CourseTypeResource.deleteCourseTypeById(9));
	}

}
