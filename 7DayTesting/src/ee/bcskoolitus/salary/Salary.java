package ee.bcskoolitus.salary;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Salary {
	private BigDecimal grossSalary;

	public BigDecimal calculateNetSalary() {
		BigDecimal netSalary = null;
		final BigDecimal SALARY_250 = BigDecimal
				.valueOf(250);
		final BigDecimal SALARY_500 = BigDecimal
				.valueOf(500);
		final BigDecimal SALARY_1000 = BigDecimal
				.valueOf(1000);
		final BigDecimal SALARY_2000 = BigDecimal
				.valueOf(2000);
		final BigDecimal INCOME_TAX_MULTIPLIER = BigDecimal
				.valueOf(0.8);
		if (grossSalary.compareTo(SALARY_1000) < 0) {
			if (grossSalary.compareTo(SALARY_500) <= 0) {
				netSalary = grossSalary;
			} else {
				netSalary = SALARY_500.add(grossSalary
						.subtract(SALARY_500)
						.multiply(INCOME_TAX_MULTIPLIER));
			}
		} else if (grossSalary.compareTo(SALARY_1000) >= 0
				&& grossSalary.compareTo(SALARY_2000) < 0) {
			netSalary = SALARY_250.add(grossSalary
					.subtract(SALARY_250)
					.multiply(INCOME_TAX_MULTIPLIER));
		} else if (grossSalary
				.compareTo(SALARY_2000) >= 0) {
			netSalary = grossSalary
					.multiply(INCOME_TAX_MULTIPLIER);
		}
		return netSalary.setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal calculateSocialTax() {
		return grossSalary.multiply(BigDecimal.valueOf(0.3))
				.setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal getGrossSalary() {
		return grossSalary;
	}

	public void setGrossSalary(BigDecimal grossSalary)
			throws IllegalArgumentException {
		if (grossSalary.compareTo(BigDecimal.ZERO) > 0) {
			this.grossSalary = grossSalary;
		} else {
			throw new IllegalArgumentException();
		}
	}
}
