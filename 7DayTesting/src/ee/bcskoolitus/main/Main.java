package ee.bcskoolitus.main;

import java.math.BigDecimal;

import ee.bcskoolitus.salary.Salary;

public class Main {

	public static void main(String[] args) {
		Salary salary = new Salary();
		salary.setGrossSalary(BigDecimal.valueOf(-1342.56));
		System.out.println("NetSalary = "
				+ salary.calculateNetSalary());
		System.out.println("Social Tax = "
				+ salary.calculateSocialTax());
	}

}
