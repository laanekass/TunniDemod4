package test.ee.bcskoolitus.salary;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import ee.bcskoolitus.salary.Salary;

public class SalaryTest {

	@Test
	public void testCalculateNetSalaryGrossLessThan500() {
		BigDecimal givenGrossSalary = BigDecimal
				.valueOf(312);
		BigDecimal expectedNetSalary = BigDecimal
				.valueOf(312);
		
		Salary testSalary = new Salary();
		testSalary.setGrossSalary(givenGrossSalary);

		Assert.assertEquals(expectedNetSalary.setScale(2),
				testSalary.calculateNetSalary());
	}

	@Test
	public void testCalculateSocialTaxWithExactNumber() {
		BigDecimal givenGrossSalary = BigDecimal
				.valueOf(1000);
		BigDecimal expectedSocialTax = BigDecimal
				.valueOf(300);
		Salary testSalary = new Salary();
		testSalary.setGrossSalary(givenGrossSalary);

		Assert.assertEquals(expectedSocialTax.setScale(2),
				testSalary.calculateSocialTax());
	}

	@Test
	public void testCalculateSocialTaxWithCommaValue() {
		BigDecimal givenGrossSalary = BigDecimal
				.valueOf(777.87);
		BigDecimal expectedSocialTax = BigDecimal
				.valueOf(233.36);
		Salary testSalary = new Salary();
		testSalary.setGrossSalary(givenGrossSalary);

		Assert.assertEquals(expectedSocialTax.setScale(2),
				testSalary.calculateSocialTax());
	}

	@Test
	public void testSetPositiveGrossSalary() {
		BigDecimal givenGrossSalary = BigDecimal
				.valueOf(1000);
		Salary testSalary = new Salary();
		testSalary.setGrossSalary(givenGrossSalary);
		Assert.assertEquals(givenGrossSalary,
				testSalary.getGrossSalary());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetNegativeGrossSalary() {
		BigDecimal givenGrossSalary = BigDecimal
				.valueOf(-1000);
		Salary testSalary = new Salary();
		testSalary.setGrossSalary(givenGrossSalary);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetZeroGrossSalary() {
		BigDecimal givenGrossSalary = BigDecimal.valueOf(0);
		Salary testSalary = new Salary();
		testSalary.setGrossSalary(givenGrossSalary);
	}

}
