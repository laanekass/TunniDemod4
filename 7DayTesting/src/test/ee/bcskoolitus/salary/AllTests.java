package test.ee.bcskoolitus.salary;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SalaryTest.class })
public class AllTests {

}
