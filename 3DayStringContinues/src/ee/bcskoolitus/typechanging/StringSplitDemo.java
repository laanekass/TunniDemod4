package ee.bcskoolitus.typechanging;

public class StringSplitDemo {

	public static void main(String[] args) {
		String textWithCityNames = "These are Estonian citys: Tallinn, Tartu, Pärnu, Kuressaare";
		System.out.println(textWithCityNames.split(":")[0]);
		String estonianCitys = textWithCityNames.split(":")[1];
		//eelmine rida pikemalt
		// String[] splitResult = textWithCityNames.split(":");
		// System.out.println(splitResult[0]);
		// String estonianCitys = splitResult[1];
		
		String[] arrayOfCityNames = estonianCitys.split(",");
		//pikalt käsitsi:
		System.out.println(arrayOfCityNames[0]);
		System.out.println(arrayOfCityNames[1]);
		System.out.println(arrayOfCityNames[2]);
		System.out.println(arrayOfCityNames[3]);
		System.out.println("-------------------------------");
		//sama asi foreach-tsükliga
		for (String city : arrayOfCityNames) { 
			System.out.println(city.replaceAll("[ai]", "eeeeee").trim());
		}
	}
}
