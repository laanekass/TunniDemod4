package ee.bcskoolitus.typechanging;

public class ConditionalStatementsIf {

	public static void main(String[] args) {
		final int ADULT_AGE = 18;
		int katisAge = 25;

		if (katisAge >= ADULT_AGE) {
			System.out.println("Kati is adult");
		} else {
			System.out.println("Kati is not grown up jet");
		}
		
		String isAdultText = (katisAge >= ADULT_AGE) ? "Kati is adult" :
			"Kati is not grown up jet";
		System.out.println("Tingimustehtega: " + isAdultText);
		

		final String SUNNY = "Päike paistab";
		final String RAINY = "Vihma sajab";
		final String SNOWY = "Lund sajab";
		String weatherForecast = SNOWY;

		if (weatherForecast.equals(SUNNY)) {
			System.out.println("Hea tuju!");
		} else if (weatherForecast.equals(RAINY)) {
			System.out.println("Vihmavari!");
		} else if (weatherForecast.equals(SNOWY)) {
			System.out.println("Soojad saapad...");
		} else {
			System.out.println("Homme on ka päev!");
		}
	}
}
