package ee.bcskoolitus.typechanging;

public class IfExercise {

	public static void main(String[] args) {
		int givenNumber = Integer.parseInt(args[0]);

		if (givenNumber % 2 == 0) {
			System.out.println("Arv " + givenNumber + " on paaris");
		} else {
			System.out.println("Arv " + givenNumber + " on paaritu");
		}

		System.out.println(
				(givenNumber % 2 == 0) ? "Arv " + givenNumber + 
						" on paaris" : "Arv " + givenNumber + 
						" on paaritu");
	}

}
