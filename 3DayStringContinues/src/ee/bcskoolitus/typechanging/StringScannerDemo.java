package ee.bcskoolitus.typechanging;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StringScannerDemo {

	public static void main(String[] args) {
		String scannerText = "Name:Heleen Maibak Name:Kati Kuusk Name:Mati Tamm";
		Scanner myScanner = new Scanner(scannerText);
		while (myScanner.hasNext()) {
			System.out.println(myScanner.next());
		}
		myScanner.close();
		System.out.println("----------------------");

		Scanner scannerByWord = new Scanner(scannerText);
		scannerByWord.useDelimiter("Name:");
		while (scannerByWord.hasNext()) {
			System.out.println(scannerByWord.next());
		}
		scannerByWord.close();
		System.out.println("------------------------");

		try (Scanner scannerFile = new Scanner(new File("testFile.txt"))
				.useDelimiter("Name:")) {
			while (scannerFile.hasNext()) {
				System.out.println(scannerFile.next());
			}
//			Logger.getLogger(StringScannerDemo.class.getName())
//			.log(Level.INFO, "Reading file was successful", new Exception());
		} catch (FileNotFoundException e) {
			Logger.getLogger(StringScannerDemo.class.getName())
				.log(Level.SEVERE, null, e);
		}
		
		final int FINGER_COUNT;
		FINGER_COUNT = 5;
	}
}
