package ee.bcskoolitus.typechanging;

public class TypeChanging {

	public static void main(String[] input) {
		int fullNumber = 18;
		// byte littleByte = fullNumber; annab vea, ei saa int-i byte-ks teha
		byte littleByte = ((Integer) fullNumber).byteValue();
		System.out.println("littleByte = " + littleByte);

		Integer fullNumber2 = 295;
		byte littleByte2 = fullNumber2.byteValue();
		System.out.println("littleByte2 = " + littleByte2);

		long longNumber = 2564897L;
		short shortNumber = ((Long) longNumber).shortValue();
		System.out.println("shortValue = " + shortNumber);

		int numberWithUnderscores = 1_202_678;
		System.out.println("numberWithUnderscores = " + numberWithUnderscores);

		String oneRandomTextWithNumbers = "286";
		int numberFromRandomText = Integer.parseInt(oneRandomTextWithNumbers);
		System.out.println("numberFromRandomText = " + numberFromRandomText);
		// byte byteFromRandomText = Byte.parseByte(oneRandomTextWithNumbers); //annab
		// vea
		// System.out.println("byteFromRandomText = " + byteFromRandomText);

		String oneRandomTextWithText = "245Hello";
		// int numberFromRandomTextWithText = Integer.parseInt(oneRandomTextWithText);
		
		short simpleShortNumber = 23;
		System.out.println("simpleShortNumber = " + simpleShortNumber);
		String textFromShortNumber = ((Short)simpleShortNumber).toString();
		System.out.println("textFromShortNumber = " + textFromShortNumber);
		
		Long simpleLongNumber = 78_965_430_765L;
		System.out.println("simpleLongNumber = " + simpleLongNumber);
		String textFromLongNumber = simpleLongNumber.toString();
		System.out.println("textfromLongNumber = " + textFromLongNumber);
	}

}
