package ee.bcskoolitus.helloworld;

public class HelloWorld2 {
	
	public static void main(String[] args) {
		int a = 5;
		{
			a = 8;
			System.out.println("Hello World " + args[0] + "!");
		}
		System.out.println("a=" + a);
	}
	
}
