package ee.bcskoolitus.arrays;

public class ArrayDemo {

	public static void main(String[] args) {
		int[] numbersArray1 = new int[5];
		System.out.println("Massivi pikkus = " + numbersArray1.length);
		System.out.println("Kolmas element enne väärtustamist = " + numbersArray1[2]);
		numbersArray1[1] = 4;
		numbersArray1[2] = 56;
		numbersArray1[4] = 10;
		System.out.println("Kolmas element peale väärtustamist = " + numbersArray1[2]);
		System.out.println("-----FOR1-------");
		for (int i = 0; i < numbersArray1.length; i++) {
			System.out.println((i + 1) + ". element on " + numbersArray1[i]);
		}
		System.out.println("----FOR2--------");
		int i = 0;
		for (; i < numbersArray1.length;) {
			System.out.println((i + 1) + ". element on " + numbersArray1[i]);
			i++;
		}
		System.out.println("-----WHILE-------");
		int index1 = 0;
		while (index1 < numbersArray1.length) {
			System.out.println((index1 + 1) + ". element on " + numbersArray1[index1]);
			index1 ++;
		}
		System.out.println("-----DO-WHILE-------");
		int index2 = 0;
		do {
			System.out.println((index2 + 1) + ". element on " + numbersArray1[index2]);
			index2 ++;
		} while (index2 < numbersArray1.length);
		
		System.out.println("-----FOR1-------");
		int counter = 1;
		for (int myNumber : numbersArray1) {
			System.out.println(counter + ". element on " + myNumber);
			counter++;
		}
		
		System.out.println("-----Summa arvutamine FOR-------");
		int summa = 0;
		for (int myNumber : numbersArray1) {
			summa += myNumber;
		}
		System.out.println("Summa = " + summa);
		
		System.out.println("------------");
		int[] numbersArray2 = { 2, 8, 42 };
		System.out.println("Massivi pikkus = " + numbersArray2.length);
		System.out.println("Kolmas element = " + numbersArray2[2]);

		boolean[] booleansArray = new boolean[8];
		System.out.println("Massivi pikkus = " + booleansArray.length);
		System.out.println("Kolmas element = " + booleansArray[2]);

		char[] charsArray = new char[3];
		System.out.println("Massivi pikkus = " + charsArray.length);
		System.out.println("Kolmas element = " + charsArray[2]);

		String[][] stringsArray1 = new String[3][5];
		System.out.println("Massivi pikkus = " + stringsArray1.length);
		System.out.println("Massivi rea pikkus = " + stringsArray1[0].length);
		stringsArray1[0][0] = "abc";
		stringsArray1[0][1] = "ABC";
		stringsArray1[0][4] = "efg";
		String[] secondRow = { "hobune", "koer", "kass", "", null };
		stringsArray1[1] = secondRow;
		System.out.println(
				"esimesest reast: " + stringsArray1[0][2] + ", " + stringsArray1[0][3] + ", " + stringsArray1[0][4]);
		System.out.println(
				"teisest reast: " + stringsArray1[1][2] + ", " + stringsArray1[1][3] + ", " + stringsArray1[1][4]);
		System.out.println("------FOR---------");
		for (int rowIndex = 0; rowIndex < stringsArray1.length; rowIndex++) {
			for (int columnIndex = 0; columnIndex < stringsArray1[rowIndex].length; columnIndex++) {
				if (columnIndex == (stringsArray1[rowIndex].length - 1)) {
					System.out.println(stringsArray1[rowIndex][columnIndex]);
				} else {
					System.out.print(stringsArray1[rowIndex][columnIndex] + ", ");
				}
			}
			// System.out.println();
		}
		
		System.out.println("------while---------");
		int rowIndex = 0;
		while(rowIndex<stringsArray1.length) {
			int columnIndex = 0;
			while(columnIndex<stringsArray1[rowIndex].length) {
				if (columnIndex == (stringsArray1[rowIndex].length - 1)) {
					System.out.println(stringsArray1[rowIndex][columnIndex]);
				} else {
					System.out.print(stringsArray1[rowIndex][columnIndex] + ", ");
				}
				columnIndex++;
			}
			rowIndex++;
		}
		System.out.println("------do-while---------");
		int rowIndex1 = 0;
		do {
			int columnIndex = 0;
			do {
				if (columnIndex == (stringsArray1[rowIndex1].length - 1)) {
					System.out.println(stringsArray1[rowIndex1][columnIndex]);
				} else {
					System.out.print(stringsArray1[rowIndex1][columnIndex] + ", ");
				}
				columnIndex++;
			} while(columnIndex<stringsArray1[rowIndex1].length);
			rowIndex1++;
		} while(rowIndex1<stringsArray1.length);
		
		System.out.println("------for-each---------");
		for(String[] row: stringsArray1) {
			for(String column:row) {
				System.out.print(column + ", ");
			}
			System.out.println();
		}
	}

}
